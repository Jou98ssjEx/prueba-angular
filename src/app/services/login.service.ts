import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserI } from '../interface/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url='https://ecovitali.presttoapp.net/Puertto/PostCalidad/Api/api/Login';

  constructor(private http: HttpClient) { }

  getData(): Observable<UserI> {
    // const URL = this.url + this.key + '&q='+ ciudad;
    return this.http.get<UserI>(this.url);
  }
}
