import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { tap } from 'rxjs';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  password = '';
  user    = '';

  erroForm =  false;

  constructor(
    private _loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  loginUser(){
    if(this.user == '' || this.password == '' ){
      this.erroForm = true;
      return;
    } 
    this.erroForm = false;

    const userLogin = {
      email: this.user,
      password: this.password,
    }
    console.log(userLogin);

    const apiData$ = this._loginService.getData();

    apiData$.subscribe( data => {
      console.log(data)
      if(data.user !== this.user && data.password !== this.password){
        console.log('Auth no valid')
      }else{
        // redireciona
        console.log('Auth valid')
        this.router.navigate(['/admin']);
      }
    });

    // console.log(apiData)
  }

}
